public class House{

	private final int fundation;
	private final int walls;

	private final int roof;
	private final int windows;
	private final int doors;



	public House(Builder builder){
		fundation = builder.fundation;
		walls = builder.walls;

		roof = builder.roof;
		windows = builder.windows;
		doors = builder.doors;
	}

	public String toString(){
		return "Бетон для фундамента " + fundation + " м3\n" +
				"Кирпич для стен " + walls + " м2\n" +
				"Черепица для кровли " + roof + " м2\n" +
				"Окна " + windows + " шт\n" +
				"Двери " + doors + " шт";
	}



	public static class Builder{
		private final int fundation;
		private final int walls;

		private int roof = 0;
		private int windows = 0;
		private int doors = 0;

		public Builder(int fundation, int walls){
			this.fundation = fundation;
			this.walls = walls;
		}

		public Builder setRoof(int roof){
			this.roof = roof;
			return this;
		}

		public Builder setWindows(int windows){
			this.windows = windows;
			return this;
		}

		public Builder setDoors(int doors){
			this.doors = doors;
			return this;
		}

		public House build(){
			return new House(this);
		}
	}



}