import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        //Заказать материалы в таком-то количестве
        House house = new House.Builder(11, 12)
                .setRoof(13)
                .setWindows(14)
                .setDoors(15)
                .build();


        System.out.println(house);

    }

}